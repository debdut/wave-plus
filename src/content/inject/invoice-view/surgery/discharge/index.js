import cssFile from './index.css'
import { getHTML } from '../../../util'

function Header (title) {
  const header = document.querySelector('.contemporary-template__header__info').cloneNode(true)
  header.childNodes[0].innerText = title

  const dateSelect = document.querySelector('.invoice-preview__body > div > section.contemporary-template__metadata > div.invoice-template-details > table > tbody > tr:nth-child(3) > td:nth-child(2) > span')
    .innerText
  const date = document.createElement('div')
  date.innerText = 'Operation Date' + ' ' + dateSelect

  header.appendChild(date)

  return getHTML(header)
}

function Customer () {
  const cust = document.querySelector('.contemporary-template__metadata__customer--billing').cloneNode(true)
  const unwantedChild = cust.childNodes[0]
  cust.removeChild(unwantedChild)

  const label = document.createElement('div')
  label.className = 'label'
  label.innerText = 'Patient'

  const custCont = document.createElement('div')
  custCont.className = 'customer'
  custCont.appendChild(label)
  custCont.appendChild(cust)

  return getHTML(custCont)
}

function Package (eye = '', bio = '') {
  let pack = document.querySelector('#Content > div > div.wv-frame__wrapper > div.wv-frame__content > div.wv-frame__content__body > div.wv-frame__content__body__main > div > div > div.invoice-view__body > div.invoice-preview > div.invoice-preview__body > div > div.contemporary-template__items > table > tbody > tr > td:nth-child(1) > strong')
    .innerText
  if (!(pack.includes('SICS') || pack.includes('PHACO') || pack.includes('Sics') || pack.includes('Phaco'))) {
    pack = ''
  }

  return `<div class="package">
  <div class="label">Package</div>
  <div class="contemporary-template__metadata__customer--billing">
    <strong class="wv-text--strong">${pack}</strong>
    <div class="desc">${eye} Eye</div>
    ${(bio.length > 0) ? `<div class="desc">Biometry ${bio}</div>`: ''}
  </div>
</div>`
}

function Doctor (doc = '') {
  return `<div class="doctor">
  <div class="label">Surgeon</div>
  <div class="contemporary-template__metadata__customer--billing">
    <strong class="wv-text--strong">${doc}</strong>
  </div>
</div>`
}

function Notes () {
  const pack = document.querySelector('#Content > div > div.wv-frame__wrapper > div.wv-frame__content > div.wv-frame__content__body > div.wv-frame__content__body__main > div > div > div.invoice-view__body > div.invoice-preview > div.invoice-preview__body > div > div.contemporary-template__items > table > tbody > tr > td:nth-child(1) > strong')
    .innerText
  let rest = ''
  if (pack.includes('SICS') || pack.includes('Sics')) {
    rest = 'One Month'
  } else if (pack.includes('PHACO') || pack.includes('Phaco')) {
    rest = 'Two Weeks'
  }
  return `<div class="notes">
  <div class="label">Notes</div>
  <div class="desc">I certify that the above mentioned patient has been operated by me under LA.
     I hereby advise the patiemt to go under the above mentioned medication, REST for ${rest} 
     and follow the guidelines attached.</div>
  </div>`
}

function parseData(str) {
  const data = {}
  const lines = str
    .trim()
    .split('\n')
    .filter(l => l.length > 0)
    .map(l => {
      const parts = l.split(' - ')
      const key = parts.shift()
      const result = parts.join(' - ')
      data[key] = result
    })
  return data
}

function template () {
  const extra = document.querySelector('.contemporary-template__memo > span:nth-child(2)')
  let data
  if (extra) {
    try {
      data = parseData(extra.innerText)
    } catch (error) {
      data = {}
    }
  }
  return `<div class="contemporary-template">
    ${Header('Discharge')}

    <div class="block grid">
      ${Customer()}
      ${Package(data['Operative Eye'], data['Biometry'])}
    </div>
    ${MedTable()}

    <div class="block grid">
      ${Notes()}
      ${Doctor(data['Surgeon'])}
    </div>

    ${InfoTable()}
  </div>`
}

function Discharge () {
  const css = cssFile.toString()
  const body = document.createElement('body')
  const html = document.createElement('html')
  const head = document.createElement('head')
  const style = document.createElement('style')

  style.type = 'text/css'
  if (style.styleSheet) {
    style.styleSheet.cssText = css
  } else {
    style.appendChild(document.createTextNode(css))
  }

  
  body.innerHTML = template()

  head.appendChild(style)
  html.appendChild(head)
  html.appendChild(body)

  return getHTML(html)
}

export default Discharge

function MedTable () {
  return `<div class="table-container block">
  <div class="label">Medicine Schedule</div>
  <table class="wv-table">
      <thead>
        <tr>
          <th class="left-align bold">Medicine</th>
          <th class="middle-align">Times</th>
          <th>Days</th>
          <th>Timing</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="left-align bold">Abdrops PD / Gate PD</td>
          <td class="middle-align">6</td>
          <td>40</td>
          <td> </td>
        </tr>
        <tr>
          <td class="left-align bold">Tropicacyl Plus / TM Plus</td>
          <td class="middle-align">2</td>
          <td>10</td>
          <td>8AM 8PM</td>
        </tr>
        <tr>
          <td class="left-align bold">Prednisolone 10mg Tablet / Cefadrox 50 Mg Tablet</td>
          <td class="middle-align">1</td>
          <td>5</td>
          <td> </td>
        </tr>
        <tr>
          <td class="left-align bold">Rantac Tablet 150mg Tablet</td>
          <td class="middle-align">1</td>
          <td>5</td>
          <td> </td>
        </tr>
        <tr>
          <td class="left-align bold">Becozyme C Forte Tablet</td>
          <td class="middle-align">1</td>
          <td>15</td>
          <td>Bed Time</td>
        </tr>
        <tr>
          <td class="left-align bold">Combiflam Tablet</td>
          <td class="middle-align"></td>
          <td>&nbsp;</td>
          <td>SOS</td>
        </tr>
        <tr>
          <td class="left-align bold">Carboxymethyl Cellulose Eye Drop</td>
          <td class="middle-align">3</td>
          <td>&nbsp;</td>
          <td> </td>
        </tr>
        <tr>
          <td class="left-align bold">Nepafenac Eye Drop</td>
          <td class="middle-align">3</td>
          <td>&nbsp;</td>
          <td> </td>
        </tr>
        <tr>
          <td> </td>
          <td> </td>
          <td> </td>
          <td> </td>
        </tr>
      </tbody>
  </table>
</div>`
}

function InfoTable () {
  return `<table class="other-table">
  <thead>
    <tr>
      <th>RDSPH</th>
      <th>DCYL </th>
      <th>AXIS </th>
      <th> VA </th>
      <th>LDSPH</th>
      <th>DCYL </th>
      <th>AXIS </th>
      <th> VA </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
  </tbody>
</table>`
}